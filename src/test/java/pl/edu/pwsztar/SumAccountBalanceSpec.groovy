package pl.edu.pwsztar

import spock.lang.Specification

class SumAccountBalanceSpec extends Specification {

    def "should return proper sum of account balances"() {
        given: "initial data"
            def bank = new Bank()
            def firstAccountNumber = bank.createAccount()
            def secondAccountNumber = bank.createAccount()
            def firstAmount = 100
            def secondAmount = 123
            bank.deposit(firstAccountNumber, firstAmount)
            bank.deposit(secondAccountNumber, secondAmount)
        when: "wants to get sum of account balances"
            def balance = bank.sumAccountsBalance()
        then: "gets proper balances sum"
            balance == firstAmount + secondAmount
    }
}
