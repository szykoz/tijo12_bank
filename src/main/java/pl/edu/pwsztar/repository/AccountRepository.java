package pl.edu.pwsztar.repository;

import pl.edu.pwsztar.entity.Account;

import java.util.Collection;
import java.util.Optional;

public interface AccountRepository {
    Account create();

    void delete(int accountNumber);

    Optional<Account> getByNumber(int accountNumber);

    Collection<Account> getAll();
}
